package com.charles.cgpacalc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    final private int[] gr = new int[11];
    final private int[] cr = new int[11];
    private Calculate calc = new Calculate();
    int count = 0;
    double cgpa;
    double tcredit = 0;
    int a = 0;
    int subjects=11;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView ttv = (TextView) findViewById(R.id.toolbarTextView);
        ttv.setVisibility(View.VISIBLE);

       final Spinner[] g = new Spinner[11];
        final Spinner[] c = new Spinner[11];
        g[0] = (Spinner) findViewById(R.id.grade1);
        g[1] = (Spinner) findViewById(R.id.grade2);
        g[2] = (Spinner) findViewById(R.id.grade3);
        g[3] = (Spinner) findViewById(R.id.grade4);
        g[4] = (Spinner) findViewById(R.id.grade5);
        g[5] = (Spinner) findViewById(R.id.grade6);
        g[6] = (Spinner) findViewById(R.id.grade7);
        g[7] = (Spinner) findViewById(R.id.grade8);
        g[8] = (Spinner) findViewById(R.id.grade9);
        g[9] = (Spinner) findViewById(R.id.grade10);
        g[10] = (Spinner) findViewById(R.id.grade11);
        c[0] = (Spinner) findViewById(R.id.credit1);
        c[1] = (Spinner) findViewById(R.id.credit2);
        c[2] = (Spinner) findViewById(R.id.credit3);
        c[3] = (Spinner) findViewById(R.id.credit4);
        c[4] = (Spinner) findViewById(R.id.credit5);
        c[5] = (Spinner) findViewById(R.id.credit6);
        c[6] = (Spinner) findViewById(R.id.credit7);
        c[7] = (Spinner) findViewById(R.id.credit8);
        c[8] = (Spinner) findViewById(R.id.credit9);
        c[9] = (Spinner) findViewById(R.id.credit10);
        c[10] = (Spinner) findViewById(R.id.credit11);

        try
        {
            Intent intent = getIntent();
            int temp = intent.getIntExtra("subjects", 0);
            subjects=temp;
            for(int i=11;i>subjects;i--)
            {
                g[i-1].setVisibility(View.INVISIBLE);
                c[i-1].setVisibility(View.INVISIBLE);
            }
        }
        catch(Exception e)
        {
            Toast.makeText(MainActivity.this,"Error passing int value",Toast.LENGTH_SHORT).show();
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingbutton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                for (int i = 0; i < subjects; i++) {
                        try {
                            gr[i] = calc.getGrade(String.valueOf(g[i].getSelectedItem()));
                            cr[i] = calc.getCredit(String.valueOf(c[i].getSelectedItem()));
                            if ((gr[i] == 0) || (cr[i] == 0)) {
                                count = 0;
                                Snackbar.make(view, "Please Select All The Fields!", Snackbar.LENGTH_SHORT)
                                        .setAction("Action", null).show();
                                break;
                            }
                            count = 1;
                        } catch (Exception e) {
                            count = 0;
                            Snackbar.make(view, "Please Select All The Fields!", Snackbar.LENGTH_SHORT)
                                    .setAction("Action", null).show();

                        }
                    }
                    if (count == 1) {
                        for (int i = 0; i < 11; i++) {
                            a = a + (gr[i] * cr[i]);
                            tcredit = tcredit + cr[i];
                        }
                        cgpa = a / tcredit;
                        Intent i = new Intent(MainActivity.this, Result.class);
                        Bundle b = new Bundle();
                        b.putDouble("key", cgpa);
                        i.putExtras(b);
                        startActivity(i);
                    }

                    if (count == 1) {
                        Snackbar.make(view, "Awesome! ", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                    }

                }

            }
        );




    }


}



