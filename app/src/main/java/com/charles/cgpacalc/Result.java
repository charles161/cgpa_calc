package com.charles.cgpacalc;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.content.Intent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;



public class Result extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle b= getIntent().getExtras();
        final java.text.DecimalFormat df= new java.text.DecimalFormat("#.##");

        final double result=b.getDouble("key");
        TextView t= (TextView) findViewById(R.id.cgpatext);
        t.setText(df.format(result));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Sharing...", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
                Intent share=new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT,"My CGPA is :\n \t "+df.format(result));
                if (share.resolveActivity(getPackageManager()) != null) {
                    startActivity(share);
                }
            }
        });
    }

}
