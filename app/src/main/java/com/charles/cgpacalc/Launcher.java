package com.charles.cgpacalc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Launcher extends AppCompatActivity {
    int sub=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final EditText subjects=(EditText) findViewById(R.id.subjects);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sub = Integer.parseInt(subjects.getText().toString());
                }
                catch(Exception e)
                {
                    sub=0;
                }
                if((sub>4)&&(sub<12)){
                    Intent i=new Intent(Launcher.this,MainActivity.class);
                    i.putExtra("subjects",sub);
                    startActivity(i);
                }
                else
                {
                    Snackbar.make(view, "Please enter a value of 5 - 11", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });
    }

}
